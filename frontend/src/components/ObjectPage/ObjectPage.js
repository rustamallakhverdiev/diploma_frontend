import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from 'axios';
import "./ObjectPage.scss";
import DeleteModal from "../DeleteModal/DeleteModal.js";
import ChoiceMyObjectModal from "../ChoiceMyObjectModal/ChoiceMyObjectModal.js";
import Edit from "../../source/images/Edit.png";
import EditBlack from "../../source/images/EditBlack.png";
import DeleteWhite from "../../source/images/DeleteWhite.png";
import DeleteBlack from "../../source/images/DeleteBlack.png";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";

const ObjectPage = () => {
  let navigate = useNavigate();
  const { id } = useParams();

  const [deleteModalActive, setDeleteModalActive] = useState(false);
  const [choiceMyObjectModalActive, setChoiceMyObjectModalActive] = useState(false);
  const [myObjects, setMyObjects] = useState([]);

  const [favourite, setFavourite] = useState(false);
  const [transaction, setTransaction] = useState(false);
  const [object, setObject] = useState([]);
  const [user, setUser] = useState([]);
  const [useEffectDo, setUseEffectDo] = useState(true);

  const { authorization } = JSON.parse(localStorage.getItem('authorization'));
  const colorTheme = localStorage.getItem('app-theme');

  useEffect(() => {
    if (useEffectDo) {
      axios.post('http://localhost:8000/object/getObjectById',
      {
        id: id
      },
      {
        headers: { authorization }
      }
      ).then(res => {
        const resultObject = res.data.object;
        setObject(resultObject);
        const resultUser = res.data.user;
        setUser(resultUser);
        const resultFavourite = res.data.favourite;
        setFavourite(resultFavourite);
        const resultTransaction = res.data.transaction;
        setTransaction(resultTransaction);
        setUseEffectDo(false);
      });
    }
  }, [useEffectDo, setUseEffectDo]);

  const onClickEdit = () => {
    navigate(`/EditObject/${object.id}`);
    window.scrollTo(0,0);
  }

  const onClickDelete = () => {
    setDeleteModalActive(true);
  }

  const onClickAddIntoFavourites = async () => {
    if (favourite) {
      setFavourite(false);
      await axios.delete('http://localhost:8000/favourite/deleteFavourite', {
        headers: { authorization, id }
      });
    } else if (!favourite) {
      setFavourite(true);
      await axios.post('http://localhost:8000/favourite/addFavourite',
      {
        id
      },
      {
        headers: { authorization }
      });
    }
  }

  const onClickGoToPersonalAccount = () => {
    navigate(`/PersonalAccount/${object.userId}`);
    window.scrollTo(0,0);
  }

  const onClickSendRequest = async () => {
    if (transaction.status) {
      const id = transaction.transactionId;
      await axios.delete('http://localhost:8000/transaction/deleteTransaction',
      {
        headers: { authorization, id }
      }
      ).then(() => {
        setTransaction({status: false, id: -1});
      });
    } else if (!transaction.status) {
      await axios.get('http://localhost:8000/object/getMyObjectsForModal',
      {
        headers: { authorization }
      }
      ).then(res => {
        const result = res.data.data;
        setMyObjects(result);
      });
      setChoiceMyObjectModalActive(true);
    }
  }

  return (
    <div className="container_object_page">
      <div className="left_block">
        <div className="title_block">
          <p className="title_data">
            {`${object.city} - ${object.categories} - ${object.title}`}
          </p>
          <div className="title_edit_delete_block">
            <p className="title_text">
              {object.title}
            </p>
            <div 
              className={(object.ownerStatus === true && object.status === true ) ? ("icon_block") : ("icon_block_false")}
            >
              <img 
                src={(colorTheme === 'dark') ? (Edit) : (EditBlack)}
                alt="Edit"
                onClick={() => onClickEdit()}
              />
              <img 
                src={(colorTheme === 'dark') ? (DeleteWhite) : (DeleteBlack)}
                alt="Delete"
                onClick={() => onClickDelete()}
              />
            </div>
          </div>
        </div>

        <div className="favourites_date_block">
          {
            (favourite === true) ? 
            (
              <div
                className={(object.ownerStatus !== true) ? ("button_delete_favourite") : ("button_favourite_false")}
                onClick={() => onClickAddIntoFavourites()}
              >
                Убрать из избранных
              </div>
            ) : 
            (
              <div
                className={(object.ownerStatus !== true) ? ("button_add_favourite") : ("button_favourite_false")}
                onClick={() => onClickAddIntoFavourites()}
              >
                Добавить в избранное
              </div>
            )
          }
          <p className="date_text">
            {object.date}
          </p>
        </div>

        <img
          className="main_img" 
          src={(object.imgName) ? (object.imgName) : (DefaultImgObject)}
          alt="Object image"
        />
      </div>

      <div className={(object.ownerStatus !== true) ? ("right_block") : ("right_block_ownerStatus_false")}>
         <div className="right_block__title-block">
          <p className="title_description">
            Описание:
          </p>
          <p className="text_description">
            {object.description}
          </p>
        </div>

        <div className={(object.ownerStatus !== true) ? ("user_block") : ("user_block_false")}>
          <div className="user_info_block">
            <div className="user_name_date_block">
              <p className="name">
                {user.fio}
              </p>
              <p className="date">
                {`На Swap c ${user.date}`}
              </p>
            </div>
            <img
              className="user_img" 
              src={(user.imgName) ? (user.imgName) : (DefaultImgObject)}
              alt="User image"
            />
          </div>

          <div
            className="button_go_personal_account"
            onClick={() => onClickGoToPersonalAccount()}
          >
            Перейти в профиль
          </div>

          <p className="hint">
            Перейди в профиль владельца, <br /> что посмотреть все его товары
          </p>
        </div>

        {
          (transaction.status === true) ? (
            <div
              className={(object.ownerStatus !== true) ? ("button_delete_request") : ("button_send_request_false")}
              onClick={() => onClickSendRequest()}
            >
              Отменить запрос на обмен
            </div>
          ) : (
            <div
              className={(object.ownerStatus !== true) ? ("button_send_request") : ("button_send_request_false")}
              onClick={() => onClickSendRequest()}
            >
              Запросить обмен
            </div>
          )
        }
      </div>
      {deleteModalActive && (
        <DeleteModal id={object.id} setDeleteModalActive={setDeleteModalActive} />
      )}
      {choiceMyObjectModalActive && (
        <ChoiceMyObjectModal myObjects={myObjects} objectRecipientId={object.id} setUseEffectDo={setUseEffectDo} setChoiceMyObjectModalActive={setChoiceMyObjectModalActive} />
      )}
    </div>
  );
};

export default ObjectPage;