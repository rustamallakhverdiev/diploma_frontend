import React from "react";
import { useNavigate } from "react-router-dom";
import "./Footer.scss";
import LogoWhite from "../../source/images/Logo.png";
import LogoBlack from "../../source/images/LogoBlack.png";

const Footer = () => {
  let navigate = useNavigate();
  const colorTheme = localStorage.getItem('app-theme');

  const toGoMainPage = () => {
    navigate("/");
    window.scrollTo(0,0);
  };

  const toGoInformation = () => {
    navigate("/Information");
    window.scrollTo(0,0);
  };

  const toGoMyObject = () => {
    navigate("/MyObjects");
    window.scrollTo(0,0);
  };

  const toGoRequests = () => {
    navigate("/Transactions");
    window.scrollTo(0,0);
  };

  return (
    <div className="container_footer">
      <div className="footer">
        <div className="logo_block">
          <img src={(colorTheme === 'dark') ? (LogoWhite) : (LogoBlack)} alt="Logo" onClick={() => toGoMainPage()} />
          <div className="logo_text_block">
            <p className="main_text">Swap</p>
            <p className="no_main_text">Обмен товарами</p>
          </div>
        </div>

        {
          localStorage.getItem("authorization") ? (
            <ul className="navbar_block">
              <a className="navbar_point" onClick={() => toGoMainPage()}>
                Главная
              </a>
              <a className="navbar_point" onClick={() => toGoMyObject()} >
                Мои товары
              </a>
              <a className="navbar_point" onClick={() => toGoRequests()} >
                Заяки на обмен
              </a>
              <a className="navbar_point" onClick={() => toGoInformation()} >
                О нас
              </a>
            </ul>
          ) : (
            <ul className="navbar_block_no_auth">
              <a className="navbar_point" onClick={() => toGoMainPage()}>
                Главная
              </a>
              <a className="navbar_point" onClick={() => toGoInformation()} >
                О нас
              </a>
            </ul>
          )
        }

        <div className="phone_number">
          <p>
            +7(928)-504-81-44
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
