import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from 'axios';
import "./EditUser.scss";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";
import AddImg from "../../source/images/AddImg.png";
import DeleteBlue from "../../source/images/DeleteBlue.png";


const EditUser = () => {
  let navigate = useNavigate();
  const { authorization } = JSON.parse(localStorage.getItem('authorization'));

  const [user, setUser] = useState({});
  const [errorText, setErrorText] = useState({
    fio: '',
    city: '',
    phoneNumber: ''
  });
  const [useEffectDo, setUseEffectDo] = useState(true);
  const [loadingImage, setLoadingImage] = useState({
    src: DefaultImgObject,
    file: ''
  });

  useEffect(() => {
    if (useEffectDo) {
      axios.get('http://localhost:8000/user/getUserByIdForEdit',
      {
        headers: { authorization }
      }
      ).then(async (res) => {
        const resultUser = res.data.data;
        await setUser(resultUser);
        setUseEffectDo(false);
        
        if (resultUser.imgName) {
          setLoadingImage({
            src: resultUser.imgName,
            file: ''
          });
        }
      });
    }
  }, [useEffectDo, setUseEffectDo]);

  let flagError = true;

  const onClickButtonEditUser = () => {
    setErrorText({
      fio: '',
      city: '',
      phoneNumber: ''
    });
    flagError = true;

    const fd = new FormData();
    if (loadingImage.src === DefaultImgObject) {
      fd.append('imgName', '');
    } else if (loadingImage.src !== DefaultImgObject && loadingImage.file !== '') {
      fd.append('imgName', loadingImage.file);
    }

    if (!user.fio.length) {
      setErrorText({
        ...errorText,
        fio: 'Заполните поле ФИО!'
      });
      flagError = false;
    }
    if (!user.city.length) {
      setErrorText({
        ...errorText,
        city: 'Заполните поле Город!'
      });
      flagError = false;
    }
    if (!user.phoneNumber) {
      setErrorText({
        ...errorText,
        phoneNumber: 'Заполните поле Телефон!'
      });
      flagError = false;
    }
    if (!/(^[8]{0,1}\d{10}$)/.test(user.phoneNumber)) {
      setErrorText({
        ...errorText,
        phoneNumber: 'Используйте формат: 89000000000!'
      });
      flagError = false;
    }
    if (!/^(?! )(?!.* $)(?!(?:.* ){4}).*$/.test(user.fio)) {
      setErrorText({
        ...errorText,
        fio: 'Недопустимое количество пробелов!'
      });
      flagError = false;
    }
    if (!/^[А-Яа-яA-Za-z ]+$/.test(user.city)) {
      setErrorText({
        ...errorText,
        city: 'Используйте только буквы!'
      });
      flagError = false;
    }
    if (!/^(?! )(?!.* $)(?!(?:.* ){3}).*$/.test(user.city)) {
      setErrorText({
        ...errorText,
        city: 'Максимум три слова!'
      });
      flagError = false;
    }

    if (flagError === true) {
      axios.patch('http://localhost:8000/user/editUser',
      {
        id: user.id,
        fio: user.fio,
        city: user.city,
        phoneNumber: user.phoneNumber
      },
      {
        headers: { authorization }
      }
      ).then(res => {
        if (loadingImage.file === '' && loadingImage.src === DefaultImgObject) {
          axios.patch('http://localhost:8000/user/deleteImgUser',
          {
            id: user.id
          });
        } else if (loadingImage.file === '' && loadingImage.src !== DefaultImgObject) {
          
        } else {
          axios.patch('http://localhost:8000/user/addImgForUser', fd, {
            headers: { id: user.id }
          });
        }

        setUser({});

        navigate(`/PersonalAccount/${user.id}`);
        window.scrollTo(0,0);
      }).catch((err) => {
        if (err.response.status === 422) return setErrorText({ ...errorText, phoneNumber: 'Неопознанная ошибка!' });
      });
    }
  }

  const onClickDeleteImg = async () => {
    await setLoadingImage({
      src: DefaultImgObject,
      file: ''
    });
  }

  const previewImg = async (e) => {
    if (e.target.files[0]) {
      await setLoadingImage({
        src: URL.createObjectURL(e.target.files[0]),
        file: e.target.files[0]
      });
    }
  }

  const onClickGoToChangePassword = () => {
    navigate('/ChangePasswordByOldAndNew');
    window.scrollTo(0,0);
  }

  return (
    <div className="container_edit_user">
      <p className="title_page">
        Редактирование личной информации
      </p>

      <div className="main_block">
        <div className="img_block">
          <img
            className="img_user"
            src={loadingImage.src}
            alt="ImgUser" />
          <div className="button_img_block">
            <label>
              <input
                accept="image/*"
                id="contained-button-file"
                name="contained-button-file"
                multiple
                type="file"
                onChange={(e) => previewImg(e)}
                value=''
                hidden
              />
              <img
                htmlFor="contained-button-file"
                src={AddImg}
                alt="AddImg"
              />
              <p>
                Добавить фото
              </p>
            </label>
            <label>
              <img
                src={DeleteBlue}
                alt="DeleteBlue"
                onClick={(e) => onClickDeleteImg(e)}
              />
              <p>
                Удалить все фото
              </p>
            </label>
          </div>
        </div>

        <div className="main_input_block">
          <div className="one_input_block">
            <input
              className="input_text"
              type="text"
              maxLength="50"
              onChange={(e) =>
                setUser({
                  ...user,
                  fio: e.target.value,
                })
              }
              value={user.fio || ''}
              placeholder="ФИО"
            />
            <p className="text_error">
              {errorText.fio}
            </p>
          </div>

          <div className="one_input_block">
            <input
              className="input_text"
              type="text"
              maxLength="50"
              onChange={(e) =>
                setUser({
                  ...user,
                  city: e.target.value,
                })
              }
              value={user.city || ''}
              placeholder="Город"
            />
            <p className="text_error">
              {errorText.city}
            </p>
          </div>

          <div className="one_input_block">
            <input
              className="input_text"
              type="text"
              maxLength="50"
              onChange={(e) =>
                setUser({
                  ...user,
                  phoneNumber: e.target.value,
                })
              }
              value={user.phoneNumber || ''}
              placeholder="Телефон"
            />
            <p className="text_error">
              {errorText.phoneNumber}
            </p>
          </div>

          <div
            className="main_input_block__button-change-password"
            onClick={() => onClickGoToChangePassword()}
          >
            Сменить пароль
          </div>
        </div>
      </div>

      <div
        className="button_save_user"
        onClick={() => onClickButtonEditUser()}
      >
        Сохранить
      </div>
    </div>
  );
};

export default EditUser;
