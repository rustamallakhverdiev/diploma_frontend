import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./MyObjects.scss";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";
import sortMyObjectWhite from "../../source/images/sortMyObjectWhite.png";
import sortMyObjectBlack from "../../source/images/sortMyObjectBlack.png";
import sortMyObjectBlue from "../../source/images/sortMyObjectBlue.png";

const MyObjects = () => {
  let navigate = useNavigate();
  const colorTheme = localStorage.getItem('app-theme');

  const [myObject, setMyObject] = useState([]);
  const [statusSort, setStatusSort] = useState('DESC');
  const [useEffectDo, setUseEffectDo] = useState(true);

  useEffect(() => {
    const { authorization } = JSON.parse(localStorage.getItem('authorization'));

    if (useEffectDo) {
      axios.post('http://localhost:8000/object/getMyObjects',
      {
        statusSort
      },
      {
        headers: { authorization }
      }
      ).then(res => {
        const result = res.data.data;
        setMyObject(result);
        setUseEffectDo(false);
      });
    }
  }, [useEffectDo, setUseEffectDo, statusSort]);
  
  const onClickSort = () => {
    if (statusSort === 'ASC') {
      setStatusSort('DESC');
    } else if (statusSort === 'DESC') {
      setStatusSort('ASC');
    }
    setUseEffectDo(true);
  }

  const onClickAddObj = () => {
    navigate("/AddObject");
    window.scrollTo(0,0);
  }

  const onClickObjectBlock = (id) => {
    navigate(`/ObjectPage/${id}`);
    window.scrollTo(0,0);
  }

  return (
    <div className="container_my_object">
      <p className="title_page">
        Мои товары
      </p>
      <div className="sort_block">
        <div
          className="button_add_object"
          onClick={() => onClickAddObj()}
        >
          Добавить товар
        </div>
        <p>
          cначала обменённые товары: 
        </p>
        <img 
          src={(statusSort === 'DESC') ? ((colorTheme === 'dark') ? (sortMyObjectWhite) : (sortMyObjectBlack)) : (sortMyObjectBlue)}
          alt="sortMyObject"
          className={(statusSort === 'DESC') ? ("img_sort_my_object") : ("img_sort_my_object_false")}
          onClick={() => onClickSort()}
        />
      </div>
      {
        myObject.length !== 0 ? (
          <div className="grid_block">
            {
              myObject.map((element, index) => (
                <div
                  key={`key-${index}`}
                  className={(element.status) ? ("object_block") : ("object_block_false")}
                  onClick={() => onClickObjectBlock(element.id)}
                >
                  <img
                    src={(element.imgName) ? (element.imgName) : (DefaultImgObject)}
                    alt="ImgObject"
                    className={(element.status) ? ("img_object") : ("img_object_false")}
                  />

                  <div className="div_info">
                    <div>
                      <p className={(element.status) ? ("title") : ("title_false")}>
                        {element.title}
                      </p>
                      <p className={(element.status) ? ("city") : ("city_false")}>
                        {element.city}
                      </p>
                    </div>
                  </div>

                  <p className={(element.status) ? ("date_add") : ("date_add_false")}>
                    {element.date}
                  </p>
                </div>
              ))
            }
          </div>
        )
        :
        (
          <p className="text_error">
            Объявления не найдены!
          </p>
        )
      }
    </div>
  );
};

export default MyObjects;