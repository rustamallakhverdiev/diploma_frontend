import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from 'axios';
import "./EditObject.scss";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";
import AddImg from "../../source/images/AddImg.png";
import DeleteBlue from "../../source/images/DeleteBlue.png";


const EditObject = () => {
  let navigate = useNavigate();
  const { id } = useParams();
  const { authorization } = JSON.parse(localStorage.getItem('authorization'));

  const categoriesСhoice = [
    {value: '', title: 'Выберите категорию'},
    {value: 'Детские игрушки', title: 'Детские игрушки'},
    {value: 'Настольные игры', title: 'Настольные игры'},
    {value: 'Книги', title: 'Книги'},
    {value: 'Аксессуары', title: 'Аксессуары'},
    {value: 'Одежда', title: 'Одежда'},
    {value: 'Обувь', title: 'Обувь'},
    {value: 'Электроника', title: 'Электроника'},
    {value: 'Инструменты', title: 'Инструменты'},
    {value: 'Мебель', title: 'Мебель'},
    {value: 'Еда', title: 'Еда'},
    {value: 'Недвижимость', title: 'Недвижимость'},
    {value: 'Транспорт', title: 'Транспорт'},
    {value: 'Другое', title: 'Другое'}
  ];

  const categoriesReceiveСhoice = [
    {value: '', title: 'Выберите категорию'},
    {value: 'Детские игрушки', title: 'Детские игрушки'},
    {value: 'Настольные игры', title: 'Настольные игры'},
    {value: 'Книги', title: 'Книги'},
    {value: 'Аксессуары', title: 'Аксессуары'},
    {value: 'Одежда', title: 'Одежда'},
    {value: 'Обувь', title: 'Обувь'},
    {value: 'Электроника', title: 'Электроника'},
    {value: 'Инструменты', title: 'Инструменты'},
    {value: 'Мебель', title: 'Мебель'},
    {value: 'Еда', title: 'Еда'},
    {value: 'Недвижимость', title: 'Недвижимость'},
    {value: 'Транспорт', title: 'Транспорт'},
    {value: 'Другое', title: 'Другое'}
  ];

  const [object, setObject] = useState({});

  const [errorTitle, setErrorTitle] = useState("");
  const [errorCategories, setErrorCategories] = useState("");
  const [errorCategoriesReceive, setErrorCategoriesReceive] = useState("");
  const [errorDescription, setErrorDescription] = useState("");

  const [useEffectDo, setUseEffectDo] = useState(true);

  const [loadingImage, setLoadingImage] = useState({
    src: DefaultImgObject,
    file: ''
  });

  useEffect(() => {
    if (useEffectDo) {
      axios.post('http://localhost:8000/object/getObjectById',
      {
        id
      },
      {
        headers: { authorization }
      }
      ).then(res => {
        const resultObject = res.data.object;
        setObject(resultObject);
        setUseEffectDo(false);
        if (resultObject.imgName !== '') {
          setLoadingImage({
            src: resultObject.imgName,
            file: ''
          });
        }
      });
    }
  }, [useEffectDo, setUseEffectDo]);

  let flagError = true;

  const onClickButtonEditObject = () => {
    setErrorTitle("");
    setErrorCategories("");
    setErrorCategoriesReceive("");
    setErrorDescription("");

    flagError = true;

    const fd = new FormData();
    if (loadingImage.src === DefaultImgObject) {
      fd.append('imgName', '');
    } else if (loadingImage.src !== DefaultImgObject && loadingImage.file !== '') {
      fd.append('imgName', loadingImage.file);
    }

    if (object.title.length === 0) {
      setErrorTitle("Заполните поле Название!");
      flagError = false;
    }
    if (object.description.length === 0) {
      setErrorDescription("Заполните поле Опиcание!");
      flagError = false;
    }
    if (object.categories === "") {
      setErrorCategories("Заполните поле Категория добавляемого товара!");
      flagError = false;
    }
    if (object.categoriesReceive === "") {
      setErrorCategoriesReceive("Заполните поле Категория товара на обмен!");
      flagError = false;
    }

    if (flagError === true) {
      axios.patch('http://localhost:8000/object/editObject',
      {
        id: object.id,
        title: object.title,
        categories: object.categories,
        categoriesReceive: object.categoriesReceive,
        description: object.description
      },
      {
        headers: { authorization }
      }
      ).then(res => {
        if (loadingImage.file === '' && loadingImage.src === DefaultImgObject) {
          axios.patch('http://localhost:8000/object/deleteImgObject',
          {
            id: object.id
          });
        } else if (loadingImage.file === '' && loadingImage.src !== DefaultImgObject) {
          
        } else {
          axios.patch('http://localhost:8000/object/addImgForObject', fd, {
            headers: { id: object.id }
          });
        }

        setObject({});

        navigate(`/ObjectPage/${object.id}`);
        window.scrollTo(0,0);
      }).catch((err) => {
        if (err.response.status === 422) return setErrorDescription('Неопознанная ошибка!');
      });
    }
  }

  const onClickDeleteImg = async () => {
    await setLoadingImage({
      src: DefaultImgObject,
      file: ''
    });
  }

  const previewImg = async (e) => {
    if (e.target.files[0]) {
      await setLoadingImage({
        src: URL.createObjectURL(e.target.files[0]),
        file: e.target.files[0]
      });
    }
  }

  return (
    <div className="container_edit_object">
      <p className="title_page">
        Редактирование товара
      </p>

      <div className="main_block">
        <div className="img_block">
          <img
            className="img_object"
            src={loadingImage.src}
            alt="ImgObject" />
          <div className="button_img_block">
            <label>
              <input
                accept="image/*"
                id="contained-button-file"
                name="contained-button-file"
                multiple
                type="file"
                onChange={(e) => previewImg(e)}
                value=''
                hidden
              />
              <img
                htmlFor="contained-button-file"
                src={AddImg}
                alt="AddImg"
              />
              <p>
                Добавить фото
              </p>
            </label>
            <label>
              <img
                src={DeleteBlue}
                alt="DeleteBlue"
                onClick={() => onClickDeleteImg()}
              />
              <p>
                Удалить все фото
              </p>
            </label>
          </div>
        </div>

        <div className="main_input_block">
          <div className="one_input_block">
            <input
              className="input_title"
              type="text"
              maxLength="50"
              onChange={(e) =>
                setObject({
                  ...object,
                  title: e.target.value,
                })
              }
              value={object.title}
              placeholder="Название"
            />
            <p className="text_error">
              {errorTitle}
            </p>
          </div>

          <div className="one_input_block">
            <select
              className={(object.categories === '') ? ("select_categories_first_option") : ("select_categories")}
              onChange={(e) =>
                setObject({
                  ...object,
                  categories: e.target.value,
                })
              }
              value={object.categories}
            >
              {
                categoriesСhoice.map((element, index) => (
                  <option value={element.value} key={`key-${index}`}>{element.title}</option>
                ))
              }
            </select>
            <p className="text_error">
              {errorCategories}
            </p>
          </div>

          <div className="one_input_block">
            <select
              className={(object.categoriesReceive === '') ? ("select_categories_first_option") : ("select_categories")}
              onChange={(e) =>
                setObject({
                  ...object,
                  categoriesReceive: e.target.value,
                })
              }
              value={object.categoriesReceive}
            >
              {
                categoriesReceiveСhoice.map((element, index) => (
                  <option value={element.value} key={`key-${index}`}>{element.title}</option>
                ))
              }
            </select>
            <p className="text_error">
              {errorCategoriesReceive}
            </p>
          </div>

          <div className="one_input_block">
            <textarea
              className="input_description"
              type="text"
              onChange={(e) =>
                setObject({
                  ...object,
                  description: e.target.value,
                })
              }
              value={object.description}
              placeholder="Описание"
            />
            <p className="text_error">
              {errorDescription}
            </p>
          </div>
        </div>
      </div>

      <div
        className="button_save_object"
        onClick={() => onClickButtonEditObject()}
      >
        Сохранить
      </div>
    </div>
  );
};

export default EditObject;
