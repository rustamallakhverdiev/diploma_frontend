import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from 'axios';
import "./TransactionPage.scss";
import RatingModal from "../RatingModal/RatingModal.js";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";
import TwoArrowWhite from "../../source/images/TwoArrowWhite.png";
import TwoArrowBlack from "../../source/images/TwoArrowBlack.png";

const TransactionPage = () => {
  let navigate = useNavigate();
  const { id } = useParams();

  const [transaction, setTransaction] = useState([]);
  const [useEffectDo, setUseEffectDo] = useState(true);
  const [userId, setUserId] = useState(-1);
  const [ratingModalActive, setRatingModalActive] = useState({
    status: false,
    objectRecipientId: -1,
    userRecipientId: -1
  });

  const { authorization } = JSON.parse(localStorage.getItem('authorization'));
  const colorTheme = localStorage.getItem('app-theme');

  useEffect(() => {
    if (useEffectDo) {
      axios.post('http://localhost:8000/transaction/getTransactionById',
      {
        id
      },
      {
        headers: { authorization }
      }
      ).then(async res => {
        const resultTrans = res.data.data;
        const resultUser = res.data.userId;
        setUserId(resultUser);
        const newResultTrans = await filterTransactionResult(resultTrans);
        await setTransaction(newResultTrans);
        setUseEffectDo(false);
      });
    }
  }, [useEffectDo, setUseEffectDo]);

  const filterTransactionResult = (result) => {
    result.forEach(item => item.user.objects = item.user.objects.filter(element => element.id === item.objectSenderId));
    return result;
  }

  const onClickGoToPersonalAccount = () => {
    if (transaction[0].object.user.id !== userId) {
      const navigateId = transaction[0].object.user.id;
      navigate(`/PersonalAccount/${navigateId}`);
      window.scrollTo(0,0);
    } else {
      const navigateId = transaction[0].user.id;
      navigate(`/PersonalAccount/${navigateId}`);
      window.scrollTo(0,0);
    }
  }

  const onClickChangeStatusTransaction = (status, id, objectRecipientId, objectSenderId) => {
    axios.patch('http://localhost:8000/transaction/changeStatusTransaction',
    {
      status,
      id,
      objectRecipientId,
      objectSenderId
    },
    {
      headers: { authorization }
    }
    ).then(() => {    
      setUseEffectDo(true);
    });
  }

  const onClickDeleteTransaction = (id) => {
    axios.delete('http://localhost:8000/transaction/deleteTransaction',
    {
      headers: { authorization, id }
    }
    ).then(() => {
      navigate('/Transactions');
      window.scrollTo(0,0);
    });
  }

  const onClickOpenRatingModal = () => {
    transaction.forEach((element) => {
      if (element.user.id !== userId) {
        setRatingModalActive({ status: true, objectRecipientId: element.user.objects[0].id, userRecipientId: element.user.id});
      } else if (element.object.user.id !== userId) {
        setRatingModalActive({ status: true, objectRecipientId: element.object.id, userRecipientId: element.object.user.id});
      }
    })
  }

  return (
    <div className="container-transactions-page trans-page">
      {
        transaction.map((element, index) => (
          <section
            className="trans-page__main-block main-block"
            key={`key-${index}`}
          >
            <div className="main-block__object-block object-block">
              <h2 className="object-block__title">
                {element.user.objects[0].title}
              </h2>
              <p className="object-block__date">
                {element.user.objects[0].date}
              </p>
              <img
                className="object-block__img"
                src={(element.user.objects[0].imgName) ? (element.user.objects[0].imgName) : (DefaultImgObject)}
              />
              <p className="object-block__description">
                {element.user.objects[0].description}
              </p>

              <div 
                className={(element.user.id !== userId) ? ("user_block") : ("user_block_false")}
              >
                <div className="user_info_block">
                  <div className="user_name_date_block">
                    <p className="name">
                      {element.user.fio}
                    </p>
                    <p className="date">
                      {`На Swap c ${element.user.date}`}
                    </p>
                  </div>
                  <img
                    className="user_img" 
                    src={(element.user.imgName) ? (element.user.imgName) : (DefaultImgObject)}
                    alt="User image"
                  />
                </div>

                <div
                  className="button_go_personal_account"
                  onClick={() => onClickGoToPersonalAccount()}
                >
                  Перейти в профиль
                </div>

                <p className="hint">
                  Перейди в профиль владельца, <br /> что посмотреть все его товары
                </p>
              </div>
            </div>

            <img
              className="main-block__two-arrow-img"
              src={(colorTheme === 'dark') ? (TwoArrowWhite) : (TwoArrowBlack)}
            />

            <div className="main-block__object-block object-block">
              <h2 className="object-block__title">
                {element.object.title}
              </h2>
              <p className="object-block__date">
                {element.object.date}
              </p>
              <img
                className="object-block__img"
                src={(element.object.imgName) ? (element.object.imgName) : (DefaultImgObject)}
              />
              <p className="object-block__description">
                {element.object.description}
              </p>

              <div 
                className={(element.object.user.id !== userId) ? ("user_block") : ("user_block_false")}
              >
                <div className="user_info_block">
                  <div className="user_name_date_block">
                    <p className="name">
                      {element.object.user.fio}
                    </p>
                    <p className="date">
                      {`На Swap c ${element.object.user.date}`}
                    </p>
                  </div>
                  <img
                    className="user_img" 
                    src={(element.object.user.imgName) ? (element.object.user.imgName) : (DefaultImgObject)}
                    alt="User image"
                  />
                </div>

                <div
                  className="button_go_personal_account"
                  onClick={() => onClickGoToPersonalAccount()}
                >
                  Перейти в профиль
                </div>

                <p className="hint">
                  Перейди в профиль владельца, <br /> что посмотреть все его товары
                </p>
              </div>
            </div>
          </section>
        ))
      }

      {
        transaction.map((element, index) => (
          (userId === element.userRecipientId && element.status === 'waiting') ? (
            <section className="trans-page__button-block button-block" key={`key-${index}`}>
              <div
                className="button-block__button"
                onClick={() => onClickChangeStatusTransaction('false', element.id, element.objectRecipientId, element.objectSenderId)}
              >
                Отклонить
              </div>
              <div
                className="button-block__button"
                onClick={() => onClickChangeStatusTransaction('true', element.id, element.objectRecipientId, element.objectSenderId)}
              >
                Принять
              </div>
            </section>
          ) : (element.status === 'waiting') ? (
            <div className="trans-page__waiting-status-block waiting-status-block" key={`key-${index}`}>
              <p className="waiting-status-block__status">
                Заявка в ожидании
              </p>
              <div
                className="waiting-status-block__button"
                onClick={() => onClickDeleteTransaction(element.id)}
              >
                Отменить запрос
              </div>
            </div>
          ) : (element.status === 'false') ? (
            <section className="trans-page__status-transaction" key={`key-${index}`}>
              Заявка отклонена
            </section>
          ) : (element.status === 'true') ? (
            <section className="trans-page__accepted-block accepted-block" key={`key-${index}`}>
              <h3 className="accepted-block__status">
                Заявка принята
              </h3>
              <span className="accepted-block__number-phone">
                {(userId !== element.user.id) ? (element.user.phoneNumber) : (element.object.user.phoneNumber)}
              </span>
              <span className="accepted-block__hint">
                Для успешного обмена созвонитесь с вашим партнером и договоритесь о месте и времени встречи
              </span>
              <div
                className={(element.object.user.id !== userId) ? ((element.object.rating) ? ("accepted-block__button--fasle") : ("accepted-block__button")) 
                : ((element.user.id !== userId) ? ((element.user.objects[0].rating) ? ("accepted-block__button--fasle") : ("accepted-block__button")) : ("accepted-block__button--fasle"))}
                onClick={() => onClickOpenRatingModal()}
              >
                Оставить отзыв
              </div>
            </section>
          ) : (<></>)
        ))
      }

      {ratingModalActive.status && (
        <RatingModal
          setUseEffectDo={setUseEffectDo}
          setRatingModalActive={setRatingModalActive}
          objectRecipientId={ratingModalActive.objectRecipientId}
          userRecipientId={ratingModalActive.userRecipientId}
        />
      )}
    </div>
  );
};

export default TransactionPage;