import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./Transactions.scss";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";
import StatusWaiting from "../../source/images/Waiting.png";
import StatusFalse from "../../source/images/Cross.png";
import StatusTrue from "../../source/images/CheckMark.png";
import TwoArrowWhite from "../../source/images/TwoArrowWhite.png";
import TwoArrowBlack from "../../source/images/TwoArrowBlack.png";

const Transactions = () => {
  let navigate = useNavigate();
  const colorTheme = localStorage.getItem('app-theme');

  const [myTransactions, setMyTransactions] = useState([]);
  const [useEffectDo, setUseEffectDo] = useState(true);

  useEffect(() => {
    const { authorization } = JSON.parse(localStorage.getItem('authorization'));

    if (useEffectDo) {
      axios.get('http://localhost:8000/transaction/getMyTransactions',
      {
        headers: { authorization }
      }
      ).then(async res => {
        const result = res.data.data;
        const newResult = await filterTransactionResult(result);
        await setMyTransactions(newResult);
        setUseEffectDo(false);
      });
    }
  }, [useEffectDo, setUseEffectDo]);

  const filterTransactionResult = (result) => {
    result.forEach(item => item.user.objects = item.user.objects.filter(element => element.id === item.objectSenderId));
    return result;
  }

  const onClickTransaction = (transactionId) => {
    navigate(`/TransactionPage/${transactionId}`);
    window.scrollTo(0,0);
  }

  return (
    <div className="container-transactions">
      <h1 className="title_page">Заявки на обмен</h1>      
      {
        myTransactions.length ? (
          <div className="trans__list-blocks">
            {
              myTransactions.map((element, index) => (
                <section 
                  key={`key-${index}`}
                  className="trans__block"
                  onClick={() => onClickTransaction(element.id)}
                >
                  <article className="trans__object">
                    <img
                      className="trans__obj-img"
                      src={(element.user.objects[0].imgName) ? (element.user.objects[0].imgName) : (DefaultImgObject)}
                    />
                    <h3 className="trans__obj-text name">
                      {element.user.objects[0].title}
                    </h3>
                    <p className="trans__obj-text">
                      {element.user.objects[0].city}
                    </p>
                    <p className="trans__obj-text">
                      {element.user.objects[0].date}
                    </p>

                    <div className="trans__status-block">
                      <span>Статус заяки:</span>
                      <p className="trans__status-block_status-text">{(element.status === 'waiting') ? ("В ожидании") : ((element.status === 'false') ? ("Отклонена") : ("Принята"))}</p>
                      <img
                        className="trans__status-block_img"
                        src={(element.status === 'waiting') ? (StatusWaiting) : ((element.status === 'false') ? (StatusFalse) : (StatusTrue))}
                      />
                    </div>
                  </article>
                  <img
                    className="trans__blocks_two-arrow-img"
                    src={(colorTheme === 'dark') ? (TwoArrowWhite) : (TwoArrowBlack)}
                  />
                  <article className="trans__object">
                    <img
                      className="trans__obj-img"
                      src={(element.object.imgName) ? (element.object.imgName) : (DefaultImgObject)}
                    />
                    <h3 className="trans__obj-text name">
                      {element.object.title}
                    </h3>
                    <p className="trans__obj-text">
                      {element.object.city}
                    </p>
                    <p className="trans__obj-text">
                     {element.object.date}
                    </p>
                  </article>
                </section>
              ))
            }
          </div>
        )
        :
        (
          <h1 className="text_error">
            У вас нет заявок!
          </h1>
        )
      }
    </div>
  );
};

export default Transactions;
