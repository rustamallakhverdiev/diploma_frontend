import React from "react";
import { useNavigate } from "react-router-dom";
import "./Header.scss";
import LogoWhite from "../../source/images/Logo.png";
import LogoBlack from "../../source/images/LogoBlack.png";
import FavouritesWhite from "../../source/images/Favourites.png";
import FavouritesBlack from "../../source/images/FavouritesBlack.png";
import UserWhite from "../../source/images/User.png";
import UserBlack from "../../source/images/UserBlack.png";
import LogoutWhite from "../../source/images/Logout.png";
import LogoutBlack from "../../source/images/LogoutBlack.png";
import LoginWhite from "../../source/images/Login.png";
import LoginBlack from "../../source/images/LoginBlack.png";

const Header = () => {
  let navigate = useNavigate();
  const colorTheme = localStorage.getItem('app-theme');

  const toGoPerAcc = () => {
    navigate(`/PersonalAccount/${-1}`);
  };

  const toGoAuth = () => {
    localStorage.removeItem('authorization');
    navigate("/Authorization");
    window.scrollTo(0,0);
  };

  const toGoMainPage = () => {
    navigate("/");
    window.scrollTo(0,0);
  };

  const toGoFavourites = () => {
    navigate("/Favourites");
    window.scrollTo(0,0);
  };

  const toGoInformation = () => {
    navigate("/Information");
    window.scrollTo(0,0);
  };

  const toGoMyObject = () => {
    navigate("/MyObjects");
    window.scrollTo(0,0);
  };

  const toGoRequests = () => {
    navigate("/Transactions");
    window.scrollTo(0,0);
  };

  return (
    <div className="container_header">
      <div className="header">
        <div className="logo_block">
          <img src={(colorTheme === 'dark') ? (LogoWhite) : (LogoBlack)} alt="Logo" onClick={() => toGoMainPage()} />
          <div className="logo_text_block">
            <p className="main_text">Swap</p>
            <p className="no_main_text">Обмен товарами</p>
          </div>
        </div>

        {
          localStorage.getItem("authorization") ? (
            <ul className="navbar_block">
              <a className="navbar_point" onClick={() => toGoMainPage()}>
                Главная
              </a>
              <a className="navbar_point" onClick={() => toGoMyObject()} >
                Мои товары
              </a>
              <a className="navbar_point" onClick={() => toGoRequests()} >
                Заяки на обмен
              </a>
              <a className="navbar_point" onClick={() => toGoInformation()} >
                О нас
              </a>
            </ul>
          ) : (
            <ul className="navbar_block_no_auth">
              <a className="navbar_point" onClick={() => toGoMainPage()}>
                Главная
              </a>
              <a className="navbar_point" onClick={() => toGoInformation()} >
                О нас
              </a>
            </ul>
          )
        }

        {localStorage.getItem("authorization") ?
          (<>
            <div className="icon_block">
              <img src={(colorTheme === 'dark') ? (FavouritesWhite) : (FavouritesBlack)} alt="Favourites" onClick={() => toGoFavourites()} />
              <img src={(colorTheme === 'dark') ? (UserWhite) : (UserBlack)} alt="User" onClick={() => toGoPerAcc()} />
              <img src={(colorTheme === 'dark') ? (LogoutWhite) : (LogoutBlack)} alt="Logout" onClick={() => toGoAuth()} />
            </div>
          </>) 
          : 
          (<>
            <div className="icon_block_no_auth">
              <img src={(colorTheme === 'dark') ? (LoginWhite) : (LoginBlack)} alt="Login" onClick={() => toGoAuth()} />
            </div>
          </>)
        }
      </div>
    </div>
  );
};

export default Header;
