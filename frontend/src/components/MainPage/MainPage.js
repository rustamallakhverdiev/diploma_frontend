import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./MainPage.scss";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";
import TwoArrowWhite from "../../source/images/TwoArrowWhite.png";

const MainPage = () => {
  let navigate = useNavigate();

  const categoriesСhoice = [
    {value: '', title: 'Выберите категорию'},
    {value: 'Детские игрушки', title: 'Детские игрушки'},
    {value: 'Настольные игры', title: 'Настольные игры'},
    {value: 'Книги', title: 'Книги'},
    {value: 'Аксессуары', title: 'Аксессуары'},
    {value: 'Одежда', title: 'Одежда'},
    {value: 'Обувь', title: 'Обувь'},
    {value: 'Электроника', title: 'Электроника'},
    {value: 'Инструменты', title: 'Инструменты'},
    {value: 'Мебель', title: 'Мебель'},
    {value: 'Еда', title: 'Еда'},
    {value: 'Недвижимость', title: 'Недвижимость'},
    {value: 'Транспорт', title: 'Транспорт'},
    {value: 'Другое', title: 'Другое'}
  ];

  const [allObject, setAllObject] = useState([]);
  const [useEffectDo, setUseEffectDo] = useState(true);
  const [filter, setFilter] = useState({
    categories: '',
    categoriesReceive: '',
    searchByObject: '',
    city: ''
  });
  const [sort, setSort] = useState('');
  const [dopFilter, setDopFilter] = useState({
    photo: false,
    ratingFour: false
  });

  useEffect(() => {
    if (useEffectDo) {
      axios.post('http://localhost:8000/object/getAllObject',
      {
        categoriesFilter: filter.categories,
        categoriesReceiveFilter: filter.categoriesReceive,
        searchByObjectFilter: filter.searchByObject,
        cityFilter: filter.city,
        valueSort: sort,
        photoFilter: dopFilter.photo,
        ratingFourFilter: dopFilter.ratingFour
      }
      ).then(res => {
        const result = res.data.data;
        const newResult = filterMassAllObject(result);
        setAllObject(newResult);
        setUseEffectDo(false);
      });
    }
  }, [useEffectDo, setUseEffectDo]);

  const filterMassAllObject = (mass) => {
    let newMass = mass.filter((item) => item.status);
    return newMass;
  }

  const onChangeCheckboxValueSort = (e) => {
    if (e.target.value === sort) {
      setSort('')
    } else {
      setSort(e.target.value)
    }
    setUseEffectDo(true);
  }

  const onChangeCheckboxPhoto = () => {
    if (dopFilter.photo === false) {
      setDopFilter({
        ...dopFilter,
        photo: true,
      })
    } else if ((dopFilter.photo === true)) {
      setDopFilter({
        ...dopFilter,
        photo: false,
      })
    }
    setUseEffectDo(true);
  }

  const onChangeCheckboxRatingFour = () => {
    if (dopFilter.ratingFour === false) {
      setDopFilter({
        ...dopFilter,
        ratingFour: true,
      })
    } else if ((dopFilter.ratingFour === true)) {
      setDopFilter({
        ...dopFilter,
        ratingFour: false,
      })
    }
    setUseEffectDo(true);
  }

  const onClickSearchButton = () => {
    setUseEffectDo(true);
  }

  const onClickSwithCategories = () => {
    let a = filter.categoriesReceive;
    let b = filter.categories;
    setFilter({
      ...filter,
      categoriesReceive: b,
      categories: a
    })
  }

  const onClickObjectBlock = (id) => {
    if (localStorage.authorization) {
      navigate(`/ObjectPage/${id}`);
      window.scrollTo(0,0);
    } else {
      alert("Авторизуйтесть для просмотра!")
    }
  }

  return (
    <div className="container_main_page">
      <div className="search_block">
        <div className="filter_block">
          <select
            className={(filter.categories === '') ? ("categories_first_option") : ("categories")}
            onChange={(e) =>
              setFilter({
                ...filter,
                categories: e.target.value,
              })
            }
            value={filter.categories}
          >
            {
              categoriesСhoice.map((element, index) => (
                <option value={element.value} key={`key-${index}`}>{element.title}</option>
              ))
            }
          </select>

          <img
            src={TwoArrowWhite} 
            alt="TwoArrowWhite" 
            className="two_arrow_buttom"
            onClick={() => onClickSwithCategories()}
          />

          <select
            className={(filter.categoriesReceive === '') ? ("categoriesReceive_first_option") : ("categoriesReceive")}
            onChange={(e) =>
              setFilter({
                ...filter,
                categoriesReceive: e.target.value,
              })
            }
            value={filter.categoriesReceive}
          >
            {
              categoriesСhoice.map((element, index) => (
                <option value={element.value} key={`key-${index}`}>{element.title}</option>
              ))
            }
          </select>

          <input
            className="search"
            type="text"
            onChange={(e) =>
              setFilter({
                ...filter,
                searchByObject: e.target.value
              })
            }
            value={filter.searchByObject}
            placeholder="Поиск по объявлениям"
          />

          <input
            className="city"
            type="text"
            onChange={(e) =>
              setFilter({
                ...filter,
                city: e.target.value
              })
            }
            value={filter.city}
            placeholder="Город"
          />

          <div 
            className="button_search"
            onClick={() => onClickSearchButton()}
          >
            Найти
          </div>
        </div>

        <div className="sort_block">
          <p className="sort_block_title">Фильтрация:</p>

          <label className="check_block">
            <input
              className="input_checkbox_none"
              type="checkbox"
              onChange={() => onChangeCheckboxPhoto()}
            />
            <div className="input_checkbox"></div>
            только с фото
          </label>

          <label className="check_block">
            <input
              className="input_checkbox_none"
              type="checkbox"
              onChange={() => onChangeCheckboxRatingFour()}
            />
            <div className="input_checkbox"></div>
            рейтинг пользователь выше 4
          </label>
        </div>

        <div className="sort_block">
          <p className="sort_block_title">Сортировка:</p>

          <label className="check_block">
            <input
              className="input_checkbox_none"
              type="checkbox"
              onChange={(e) => onChangeCheckboxValueSort(e)}       
              value='desc'
              checked={(sort === 'desc') ? true : false}
            />
            <div className="input_checkbox"></div>
            сначала новые
          </label>

          <label className="check_block">
            <input
              className="input_checkbox_none"
              type="checkbox"
              onChange={(e) => onChangeCheckboxValueSort(e)}
              value='asc'
              checked={(sort === 'asc') ? true : false}
            />
            <div className="input_checkbox"></div>
            сначала старые
          </label>

          <label className="check_block">
            <input
              className="input_checkbox_none"
              type="checkbox"
              value='rating'
              onChange={(e) => onChangeCheckboxValueSort(e)}
              checked={(sort === 'rating') ? true : false}
            />
            <div className="input_checkbox"></div>
            сначала с высоким рейтингом
          </label>
        </div>
      </div>

      {
        (allObject.length) ? (
          <div className="grid_block">
            {
              allObject.map((element, index) => (
                (element.status) ? (
                <div
                  key={`key-${index}`}
                  className="object_block"
                  onClick={() => onClickObjectBlock(element.id)}
                >
                  <img src={(element.imgName) ? (element.imgName) : (DefaultImgObject)} alt="ImgObject" />

                  <div className="div_info">
                    <div>
                      <p className="title">
                        {element.title}
                      </p>
                      <p className="city">
                        {element.city}
                      </p>
                    </div>
                  </div>

                  <p className="date_add">
                    {element.date}
                  </p>
                </div>)
                : <div className="status_false" key={`key-${index}`} ></div>
              ))
            }
          </div>
        )
        :
        (
          <p className="text_error">
            Объявления не найдены!
          </p>
        )
      }
    </div>
  );
};

export default MainPage;