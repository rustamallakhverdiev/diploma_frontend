import React, { useState } from "react";
import axios from 'axios';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';
import "./RatingModal.scss";

const RatingModal = ({ setUseEffectDo, setRatingModalActive, objectRecipientId, userRecipientId }) => {
  const [rating, setRating] = useState({
    comment: '',
    rating: 0,
    errorText: ''
  });
  const { authorization } = JSON.parse(localStorage.getItem('authorization'));

  const onClickSend = () => {
    if (rating.comment && rating.rating) {
      axios.post('http://localhost:8000/rating/createRating',
        {
          comment: rating.comment,
          rating: rating.rating,
          objectRecipientId,
          userRecipientId
        },
        {
          headers: { authorization }
        }
        ).then(() => {
          setRatingModalActive({status: false, objectRecipientId: -1, userRecipientId: -1});
          setUseEffectDo(true)
        });
    } else {
      setRating({
        ...rating,
        errorText: 'Введите комментарий и рейтинг!'
      });
    }
  }

  const onClickCancel = () => {
    setRatingModalActive({status: false, objectRecipientId: -1, userRecipientId: -1});
  }

  const onClickStar = (number) => {
    setRating({
      ...rating,
      rating: number
    });
  }

  return (
    <div
      className="rating_modal"
      onClick={() => onClickCancel()}
    >
      <div
        className="rating_modal__container_rating_modal"
        onClick={(e) => e.stopPropagation()}
      >
        <p className="container_rating_modal__title">
          Написание отзыва
        </p>

        <div>
          <textarea
            className="container_rating_modal__comment"
            type="text"
            onChange={(e) =>
              setRating({
                ...rating,
                comment: e.target.value,
              })
            }
            value={rating.comment}
            placeholder="Напишите отзыв"
          />
          <p className="container_rating_modal__error">
            {rating.errorText}
          </p>
        </div>

        <Box
          sx={{
            '& > legend': { mt: 0 },
          }}
        >
          <Rating
            name="simple-controlled"
            size="large"
            value={rating.rating}
            onChange={(event, newValue) => {
              setRating({
                ...rating,
                rating: newValue
              });
            }}
          />
        </Box>

        <div className="container_rating_modal__button_block">
          <div
            className="button_block__button_cancel"
            onClick={() => onClickCancel()}
          >
            Отмена
          </div>
          <div
            className="button_block__button_send"
            onClick={() => onClickSend()}
          >
            Отправить
          </div>
        </div>
      </div>
    </div>
  );
};

export default RatingModal;