import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./CheckLoginAndPhoneNumberToChangePassword.scss";
import Visibility from "../../source/images/Visibility.png";
import NotVisibility from "../../source/images/NotVisibility.png";

const CheckLoginAndPhoneNumberToChangePassword = () => {
  let navigate = useNavigate();

  const [data, setData] = useState({
    login: '',
    phoneNumber: ''
  });
  const [loginError, setLoginError] = useState('');
  const [phoneNumberError, setPhoneNumberError] = useState('');

  const onClickSendData = () => {
    setLoginError('');
    setPhoneNumberError('');
    let flagError = true;

    if (!data.login.length) {
      setLoginError('Заполните поле Логин!');
      flagError = false;
    }
    if (!/(^[8]{0,1}\d{10}$)/.test(data.phoneNumber)) {
      setPhoneNumberError("Используйте формат: 89000000000!");
      flagError = false;
    }
    if (!data.phoneNumber.length) {
      setPhoneNumberError('Заполните поле Телефон!');
      flagError = false;
    }

    if (flagError) {
      axios.post('http://localhost:8000/user/checkLoginAndPhoneNumberToChangePassword',
      {
        login: data.login,
        phoneNumber: data.phoneNumber
      }
      ).then((res) => {
        setLoginError('');
        setPhoneNumberError('');
        setData({ login: '', phoneNumber: '' });
        navigate(`/ChangePasswordByLoginAndPhoneNumber/${res.data.data}`);
        window.scrollTo(0,0);   
      }).catch((err) => {
        if (err.response.status === 421) return setPhoneNumberError('Неправильные данные!');   
        if (err.response.status === 422 || err.response.status === 400) return setPhoneNumberError('Неопознанная ошибка!');
      });
    }
  };

  return (
    <section className="container-check-login-and-phoneNumber-to-change-password">
      <span className="container-check-login-and-phoneNumber-to-change-password__title">
        Смена пароля
      </span>

      <div className="container-check-login-and-phoneNumber-to-change-password__input-block input-block">
        <span className="input-block__hint">
          Введите ваш логин:
        </span>
        <input
          className="input-block__input"
          maxLength="50"
          type="text"
          onChange={ (e) => setData({ ...data, login: e.target.value}) }
          value={data.login}
          placeholder="Логин"
        />
        <span className="input-block__error-text">
          {loginError}
        </span>
      </div>

      <div className="container-check-login-and-phoneNumber-to-change-password__input-block input-block">
        <span className="input-block__hint">
          Введите ваш номер телефона:
        </span>
        <input
          className="input-block__input"
          maxLength="50"
          type="text"
          onChange={ (e) => setData({ ...data, phoneNumber: e.target.value}) }
          value={data.phoneNumber}
          placeholder="Телефон"
        />
        <span className="input-block__error-text">
          {phoneNumberError}
        </span>
      </div>

      <div
        className="container-check-login-and-phoneNumber-to-change-password__button-send"
        onClick={() => onClickSendData()}
      >
        Отправить
      </div>
    </section>
  );
};

export default CheckLoginAndPhoneNumberToChangePassword;