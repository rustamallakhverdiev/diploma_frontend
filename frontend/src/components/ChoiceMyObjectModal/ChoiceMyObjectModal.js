import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./ChoiceMyObjectModal.scss";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";

const ChoiceMyObjectModal = ({ myObjects, objectRecipientId, setUseEffectDo, setChoiceMyObjectModalActive }) => {
  const { authorization } = JSON.parse(localStorage.getItem('authorization'));

  const [objectSenderId, setObjectSenderId] = useState();

  const newMyObjects = myObjects.filter((item) => item.status);

  const onClickCancel = () => {
    setChoiceMyObjectModalActive(false);
  }

  const onClickObjectBlock = (id) => {
    setObjectSenderId(id);
  }

  const onClickSend = () => {
    axios.post('http://localhost:8000/transaction/createTransaction',
    {
      objectRecipientId,
      objectSenderId
    },
    {
      headers: { authorization }
    }
    ).then(res => {
      const resultRequest = res.data.data;
      setUseEffectDo(true);
      setChoiceMyObjectModalActive(false);
    });
  }

  return (
    <div 
      className="choice_my_object_modal"
      onClick={() => onClickCancel()}
    >
      <div
        className="container_choice_my_object_modal"
        onClick={(e) => e.stopPropagation()}
      >
        <p className="title">
          Выберите ваш товар
        </p>
        {
          newMyObjects.length ? (
            <div className="grid_block">
              {
                newMyObjects.map((element, index) => (
                  <div
                    key={`key-${index}`}
                    className={(objectSenderId === element.id) ? ("object_block_selected") : ("object_block")}
                    onClick={() => onClickObjectBlock(element.id)}
                  >
                    <img
                      src={(element.imgName) ? (element.imgName) : (DefaultImgObject)}
                      alt="ImgObject"
                      className="img_object"
                    />

                    <div className="div_info">
                      <div>
                        <p className="title">
                          {element.title}
                        </p>
                        <p className="city">
                          {element.city}
                        </p>
                      </div>
                    </div>

                    <p className="date_add">
                      {element.date}
                    </p>
                  </div>
                ))
              }
            </div>
          )
          :
          (
            <p className="text_error">
              У вас нет товаров!
            </p>
          )
        }

        <div className="button_block">
          <div
            className="button_cancel"
            onClick={() => onClickCancel()}
          >
            Закрыть
          </div>
          {
            myObjects.length ?
            (
              <div
                className="button_send"
                onClick={() => onClickSend()}
              >
                Отправить
              </div>
            )
            :
            (
              <></>
            )
          }
        </div>
      </div>
    </div>
  );
};

export default ChoiceMyObjectModal;