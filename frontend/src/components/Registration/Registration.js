import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./Registration.scss";
import Visibility from "../../source/images/Visibility.png";
import NotVisibility from "../../source/images/NotVisibility.png";

const Registration = () => {
  let navigate = useNavigate();

  const [newUser, setNewUser] = useState({
    login: "",
    password: "",
    fio: "",
    phoneNumber: "",
    city: ""
  });
  const [errorLogin, setErrorLogin] = useState("");
  const [errorPassword, setErrorPassword] = useState("");
  const [errorFio, setErrorFio] = useState("");
  const [errorPhoneNumber, setErrorPhoneNumber] = useState("");
  const [errorCity, setErrorCity] = useState("");
  const [visibilityPassword, setVisibilityPassword] = useState(false);
  
  let flagError = true;

  const visibilityPasswordChange = () => {
    if (visibilityPassword) {
      setVisibilityPassword(false);
    } else {
      setVisibilityPassword(true);
    }
  }

  const toGoAuth = () => {
    localStorage.removeItem('authorization');
    navigate("/Authorization");
    window.scrollTo(0,0);
  };
  
  const register = () => {
    setErrorLogin("");
    setErrorPassword("");
    setErrorFio("");
    setErrorPhoneNumber("");
    setErrorCity("");

    flagError = true;

    if (newUser.login.length === 0) {
      setErrorLogin("Заполните поле Логин!");
      flagError = false;
    }
    if (newUser.password.length === 0) {
      setErrorPassword("Заполните поле Пароль!");
      flagError = false;
    }
    if (newUser.fio.length === 0) {
      setErrorFio("Заполните поле ФИО!");
      flagError = false;
    }
    if (newUser.phoneNumber.length === 0) {
      setErrorPhoneNumber("Заполните поле Телефон!");
      flagError = false;
    }
    if (newUser.city.length === 0) {
      setErrorCity("Заполните поле Город!");
      flagError = false;
    }

    if (!/^[A-Za-z0-9@\.]+$/.test(newUser.login)) {
      setErrorLogin("Используйте только английские буквы!");
      flagError = false;
    } else if (!/^\S*$/.test(newUser.login)) {
      setErrorLogin("Логин не должен содержать пробелы!");
      flagError = false;
    }
    if (!/(^[8]{0,1}\d{10}$)/.test(newUser.phoneNumber)) {
      setErrorPhoneNumber("Используйте формат: 89000000000!");
      flagError = false;
    }
    if (!/^(?! )(?!.* $)(?!(?:.* ){4}).*$/.test(newUser.fio)) {
      setErrorFio("Недопустимое количество пробелов!");
      flagError = false;
    }
    if (!/^[А-Яа-яA-Za-z ]+$/.test(newUser.city)) {
      setErrorCity("Используйте только буквы!");
      flagError = false;
    }
    if (!/^(?! )(?!.* $)(?!(?:.* ){3}).*$/.test(newUser.city)) {
      setErrorCity("Максимум три слова!");
      flagError = false;
    }
    

    if (flagError === true) {
      axios.post('http://localhost:8000/user/registration', {
        login: newUser.login,
        password: newUser.password,
        fio: newUser.fio,
        phoneNumber: newUser.phoneNumber,
        city: newUser.city
      }).then(async res => {
        localStorage.setItem('authorization', JSON.stringify(res.data));

        await setNewUser({
          login: "",
          password: "",
          fio: "",
          phoneNumber: "",
          city: ""
        });

        navigate("/");
        window.scrollTo(0,0);
      }).catch((err) => {
        if (err.response.status === 421) return setErrorLogin('Пользователь с таким логином уже существует!');
        if (err.response.status === 423) return setErrorPassword('Пароль должен быть длиннее 8 символов!');
        if (err.response.status === 424) return setErrorPhoneNumber('Пользователь с таким номером телефона уже существует!');
        if (err.response.status === 422 || err.response.status === 400) return setErrorCity('Неопознанная ошибка!');
      });
    }
  };

  return (
    <div className="container_registration">
      <p className="heading">Регистрация</p>

      <div className="input_block">
        <div className="input_and_error">
          <input
            maxLength="50"
            className="input"
            type="text"
            onChange={(e) =>
              setNewUser({
                ...newUser,
                login: e.target.value,
              })
            }
            value={newUser.login}
            placeholder="Логин"
          />

          <p className="error_text">
            {errorLogin}
          </p>
        </div>

        <div className="input_and_error">
          <div className="input_password_block">
            <input
              className="input_password"
              maxLength="50"
              type={visibilityPassword ? ("text") :  ("password")}
              onChange={(e) =>
                setNewUser({
                  ...newUser,
                  password: e.target.value,
                })
              }
              value={newUser.password}
              placeholder="Пароль"
            />
            <>
              {!visibilityPassword ? 
                <img src={Visibility} alt="Visibility" onClick={() => visibilityPasswordChange()} />
                :
                <img src={NotVisibility} alt="NotVisibility" onClick={() => visibilityPasswordChange()} />}
            </>
          </div>
          
          <p className="error_text">
            {errorPassword}
          </p>
        </div>

        <div className="input_and_error">
          <input
            className="input"
            type="text"
            maxLength="50"
            onChange={(e) =>
              setNewUser({
                ...newUser,
                fio: e.target.value,
              })
            }
            value={newUser.fio}
            placeholder="ФИО"
          />
          
          <p className="error_text">
            {errorFio}
          </p>
        </div>

        <div className="input_and_error">
          <input
            className="input"
            type="text"
            maxLength="50"
            onChange={(e) =>
              setNewUser({
                ...newUser,
                phoneNumber: e.target.value,
              })
            }
            value={newUser.phoneNumber}
            placeholder="Телефон"
          />
          
          <p className="error_text">
            {errorPhoneNumber}
          </p>
        </div>

        <div className="input_and_error">
          <input
            className="input"
            type="text"
            maxLength="50"
            onChange={(e) =>
              setNewUser({
                ...newUser,
                city: e.target.value,
              })
            }
            value={newUser.city}
            placeholder="Город"
          />
          
          <p className="error_text">
            {errorCity}
          </p>
        </div>
      </div>

      <div className="button_register" onClick={() => register()}>
        Зарегистрироваться
      </div>

      <a onClick={() => toGoAuth()}>
        Авторизация
      </a>
    </div>
  );
};

export default Registration;