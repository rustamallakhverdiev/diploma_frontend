import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./CheckLoginToChangePassword.scss";

const CheckLoginToChangePassword = () => {
  let navigate = useNavigate();
  const { authorization } = JSON.parse(localStorage.getItem('authorization'));

  const [login, setLogin] = useState("");
  const [loginError, setLoginError] = useState("");

  const onClickSendLogin = () => {
    setLoginError("");
    let flagError = true;

    if (!login.length) {
      setLoginError("Заполните поле Логин!");
      flagError = false;
    }

    if (flagError) {
      axios.post('http://localhost:8000/user/checkLoginToChangePassword',
      {
        login
      },
      {
        headers: { authorization }
      }
      ).then(() => {
        setLoginError("");
        setLogin("")
        navigate("/ChangePasswordByNew");
        window.scrollTo(0,0);   
      }).catch((err) => {
        if (err.response.status === 421) return setLoginError('Вы не авторизированы!');
        if (err.response.status === 423) return setLoginError('Неправильный логин!');
        if (err.response.status === 422) return setLoginError('Неопознанная ошибка!');
      });
    }
  };

  return (
    <section className="container-check-login-to-change-password">
      <span className="container-check-login-to-change-password__title">
        Смена пароля
      </span>

      <div className="container-check-login-to-change-password__input-block input-block">
        <span className="input-block__hint">
          Введите ваш логин:
        </span>
        <input
            className="input-block__input-login"
            maxLength="50"
            type="text"
            onChange={(e) => setLogin(e.target.value)}
            value={login}
            placeholder="Логин"
          />
        <span className="input-block__error-text">
          {loginError}
        </span>
      </div>

      <div
        className="container-check-login-to-change-password__button-send"
        onClick={() => onClickSendLogin()}
      >
        Отправить
      </div>
    </section>
  );
};

export default CheckLoginToChangePassword;