import React from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./DeleteModal.scss";

const DeleteModal = ({ id, setDeleteModalActive }) => {
  let navigate = useNavigate();
  const { authorization } = JSON.parse(localStorage.getItem('authorization'));

  const onClickCancel = () => {
    setDeleteModalActive(false);
  }

  const onClickDelete = () => {
    axios.delete('http://localhost:8000/object/deleteObject',
      {
        headers: { authorization, id }
      }
      ).then(() => {
        setDeleteModalActive(false);
        navigate("/MyObjects");
        window.scrollTo(0,0);
      });
  }

  return (
    <div 
      className="modal_delete"
      onClick={() => onClickCancel()}
    >
      <div
        className="container_delete_modal"
        onClick={(e) => e.stopPropagation()}
      >
        <p className="title">
          Удаление товара
        </p>
        <div className="button_block">
          <div
            className="button_cancel"
            onClick={() => onClickCancel()}
          >
            Отмена
          </div>
          <div
            className="button_delete"
            onClick={() => onClickDelete()}
          >
            Удалить
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeleteModal;