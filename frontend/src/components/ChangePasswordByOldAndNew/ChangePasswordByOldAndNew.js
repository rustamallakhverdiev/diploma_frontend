import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./ChangePasswordByOldAndNew.scss";
import Visibility from "../../source/images/Visibility.png";
import NotVisibility from "../../source/images/NotVisibility.png";

const ChangePasswordByOldAndNew = () => {
  let navigate = useNavigate();
  const { authorization } = JSON.parse(localStorage.getItem('authorization'));

  const [password, setPassword] = useState({
    new: '',
    old: ''
  });
  const [passwordErrorOld, setPasswordErrorOld] = useState('');
  const [passwordErrorNew, setPasswordErrorNew] = useState('');
  const [visibilityPassword, setVisibilityPassword] = useState({
    new: false,
    old: false
  });

  const visibilityOldPasswordChange = () => {
    if (visibilityPassword.old) {
      setVisibilityPassword({
        ...visibilityPassword,
        old: false
      });
    } else {
      setVisibilityPassword({
        ...visibilityPassword,
        old: true
      });
    }
  };

  const visibilityNewPasswordChange = () => {
    if (visibilityPassword.new) {
      setVisibilityPassword({
        ...visibilityPassword,
        new: false
      });
    } else {
      setVisibilityPassword({
        ...visibilityPassword,
        new: true
      });
    }
  };

  const onClickSendPassword = () => {
    setPasswordErrorOld('');
    setPasswordErrorNew('');
    let flagError = true;

    if (!password.old.length) {
      setPasswordErrorOld('Заполните поле для старого пароля!');
      flagError = false;
    }
    if (!password.new.length) {
      setPasswordErrorNew('Заполните поле для нового пароля!');
      flagError = false;
    }

    if (flagError) {
      axios.patch('http://localhost:8000/user/changePasswordByOldAndNew',
      {
        oldPassword: password.old,
        newPassword: password.new
      },
      {
        headers: { authorization }
      }
      ).then((res) => {
        setPasswordErrorOld('');
        setPasswordErrorNew('');
        setPassword({ new: '', old: '' });
        navigate(`/PersonalAccount/${res.data.data}`);
        window.scrollTo(0,0);   
      }).catch((err) => {
        if (err.response.status === 421) return setPasswordErrorNew('Вы не авторизированы!');
        if (err.response.status === 423 && err.response.data === 'Wrong old password!') return setPasswordErrorOld('Неправильный пароль!');
        if (err.response.status === 423 && err.response.data === 'The new password must be longer than 8 characters!') return setPasswordErrorNew('Пароль должен быть длиннее 8 символов!');
        if (err.response.status === 422 || err.response.status === 400) return setPasswordErrorNew('Неопознанная ошибка!');
      });
    }
  };

  const onClickForgotPassword = () => {
    navigate("/CheckLoginToChangePassword");
    window.scrollTo(0,0);
  };

  return (
    <section className="container-change-password-by-old-and-new">
      <span className="container-change-password-by-old-and-new__title">
        Смена пароля
      </span>

      <div className="container-change-password-by-old-and-new__input-block input-block">
        <span className="input-block__hint">
          Введите старый пароль:
        </span>
        <div className="input-block__input-password-block input-password-block">
          <input
            className="input-password-block__input-password"
            maxLength="50"
            type={(visibilityPassword.old) ? ("text") : ("password")}
            onChange={ (e) => setPassword({ ...password, old: e.target.value}) }
            value={password.old}
            placeholder="Пароль"
          />
          <>
            {
              (!visibilityPassword.old) ? (
                <img src={Visibility} alt="Visibility" onClick={ () => visibilityOldPasswordChange() } />
              ) : (
                <img src={NotVisibility} alt="NotVisibility" onClick={ () => visibilityOldPasswordChange() } />
              )
            }
          </>
        </div>
        <span className="input-block__error-text">
          {passwordErrorOld}
        </span>
      </div>

      <div className="container-change-password-by-old-and-new__input-block input-block">
        <span className="input-block__hint">
          Введите новый пароль:
        </span>
        <div className="input-block__input-password-block input-password-block">
          <input
            className="input-password-block__input-password"
            maxLength="50"
            type={(visibilityPassword.new) ? ("text") : ("password")}
            onChange={ (e) => setPassword({ ...password, new: e.target.value}) }
            value={password.new}
            placeholder="Пароль"
          />
          <>
            {
              (!visibilityPassword.new) ? (
                <img src={Visibility} alt="Visibility" onClick={ () => visibilityNewPasswordChange() } />
              ) : (
                <img src={NotVisibility} alt="NotVisibility" onClick={ () => visibilityNewPasswordChange() } />
              )
            }
          </>
        </div>
        <span className="input-block__error-text">
          {passwordErrorNew}
        </span>
      </div>

      <div
        className="container-change-password-by-old-and-new__button-send"
        onClick={() => onClickSendPassword()}
      >
        Отправить
      </div>

      <a
        className="container-change-password-by-old-and-new__forgot-password"
        onClick={() => onClickForgotPassword()}
      >
        Забыли пароль?
      </a>
    </section>
  );
};

export default ChangePasswordByOldAndNew;