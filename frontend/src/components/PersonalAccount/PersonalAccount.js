import React, {useEffect, useState} from "react";
import { useNavigate, useParams } from "react-router-dom";
import StarRatings from 'react-star-ratings';
import axios from 'axios';
import "./PersonalAccount.scss";
import { useTheme } from "../../hooks/UseTheme.js";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";
import MoonBlack from "../../source/images/MoonBlack.png";
import SunWhite from "../../source/images/SunWhite.png";

const PersonalAccount = () => {
  let navigate = useNavigate();
  const { id } = useParams();

  const [user, setUser] = useState([]);
  const [comment, setComment] = useState([]);
  const [userStatus, setUserStatus] = useState(null);
  const [useEffectDo, setUseEffectDo] = useState(true);

  const { authorization } = JSON.parse(localStorage.getItem('authorization'));
  const colorTheme = localStorage.getItem('app-theme');
  const { theme, setTheme } = useTheme();

  useEffect(() => {
      axios.post('http://localhost:8000/user/getUserById',
      {
        id
      },
      {
        headers: { authorization }
      }
      ).then(res => {
        const resultUser = res.data.data;
        const resultUserStatus = res.data.userStatus;

        const newResultUserComments = filterMassUserComments(resultUser);
        setComment(newResultUserComments);

        const newResultUser = filterMassUser(resultUser);
        setUser(newResultUser);

        setUserStatus(resultUserStatus);
        setUseEffectDo(false);
      });
  }, [id]);

  const filterMassUserComments = (mass) => {
    let massNew = [];
    mass.forEach((element1) => {
      element1.objects.forEach((element2) => {
        if (element2.rating) {
          massNew.push(element2.rating);
        }
      });
    });
    return massNew;
  };

  const filterMassUser = (mass) => {
    mass.forEach((element1) => {
      element1.objects = element1.objects.filter((item) => item.status);
    })
    return mass;
  };

  const onClickGoToObjectPage = (id) => {
    navigate(`/ObjectPage/${id}`);
    window.scrollTo(0,0);
  };

  const onClickGoToChangeInfoUser = (id) => {
    navigate('/EditUser');
    window.scrollTo(0,0);
  };

  const onClickGoToPersonalAccount = (id) => {
    navigate(`/PersonalAccount/${id}`);
    setUseEffectDo(true);
    window.scrollTo(0,0);
  };

  const onClickChangTheme = () => {
    if (colorTheme === 'dark') {
      setTheme('light');
    } else if (colorTheme === 'light') {
      setTheme('dark');
    }
    window.location.reload();
  };

  return (
    <div className="container__personal-account">
      <section
        className="personal-account__buttons-theme-block"
        onClick={() => onClickChangTheme()}
      >
        <img
          className="buttons-theme-block__icon"
          src={(colorTheme === 'dark') ? (SunWhite) : (MoonBlack)}
        />
      </section>

      <h1 className="personal-account__title">
        Личный кабинет
      </h1>

      {
        user.map((element, index) => (
          <section className="personal-account__person-block person-block" key={`key-${index}`}>
            <div className="person-block__img-and-person-info-block">
              <img
                className="person-block__person-img"
                src={(element.imgName) ? (element.imgName) : (DefaultImgObject)}
                key={`key-${index}`}
              />
              <div className="person-block__person-info-block">
                <span className="person-info-block___name">
                  {element.fio}
                </span>
                <span className="person-info-block___date">
                  {`На Swap c ${element.date}`}
                </span>
                <span className="person-info-block___city">
                  {element.city}
                </span>
              </div>
            </div>

            <div className="person-block__rating-block">
              <span className="rating-block__number">
                {`${element.rating}.0`}
              </span>
              <StarRatings
                starRatedColor="#FFD240"
                rating={element.rating}
                starDimension="27px"
                starSpacing="2px"
              />
            </div>

            <div
              className={(userStatus) ? ("rating-block__button-change-info-person") : ("rating-block__button-change-info-person--false")}
              onClick={() => onClickGoToChangeInfoUser(element.id)}
            >
              Редактировать
            </div>
          </section>
        ))
      }
    
      <h2 className="personal-account__title">
        Товары пользователя
      </h2>

      {
        user.map((elenet3, index3) => (
          (elenet3.objects.length) ? (
            <section className="grid_block" key={`key-${index3}`}>
              {
                user.map((element1) => (
                  element1.objects.map((element2, index2) => (
                    (element2.status) ? (
                      <div
                        key={`key-${index2}`}
                        className="object_block"
                        onClick={() => onClickGoToObjectPage(element2.id)}
                      >
                        <img src={(element2.imgName) ? (element2.imgName) : (DefaultImgObject)} alt="ImgObject" />

                        <div className="div_info">
                          <div>
                            <p className="title">
                              {element2.title}
                            </p>
                            <p className="city">
                              {element2.city}
                            </p>
                          </div>
                        </div>

                        <p className="date_add">
                          {element2.date}
                        </p>
                      </div>
                    )
                    : (<div className="status_false" key={`key-${index2}`}></div>)
                  ))
                ))
              }
            </section>
          ) : (
            <p className="text_error grid" key={`key-${index3}`}>
              Товары отсутствуют!
            </p>
          )
        ))
      }

      <h2 className="personal-account__title comment">
        Отзывы
      </h2>

      {
        (comment.length) ? (
          <section className="personal-account__comments-block comments-block">
            {
              comment.map((element, index) => (
                <article className="comments-block__comment comment" key={`key-${index}`}>
                  <div className="comment__person-block person-block">
                    <img
                      className="person-block__img"
                      src={(element.user.imgName) ? (element.user.imgName) : (DefaultImgObject)}
                    />
                    <div className="person-block__info-block info-block">
                      <p
                        className="info-block__name"
                        onClick={() => onClickGoToPersonalAccount(element.user.id)}
                      >
                        {element.user.fio}
                      </p>
                      <p className="info-block__date">
                        {element.date}
                      </p>
                    </div>
                  </div>

                  <StarRatings
                    starRatedColor="#FFD240"
                    rating={element.rating}
                    starDimension="27px"
                    starSpacing="2px"
                  />

                  <p className="comment__text">
                    {element.comment}
                  </p>
                </article>
              ))
            }
          </section>
        ) : (
          <p className="text_error">
            Отзывы отсутствуют!
          </p>
        )
      }
    </div>
  );
};

export default PersonalAccount;
