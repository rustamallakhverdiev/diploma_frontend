import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./Authorization.scss";
import Visibility from "../../source/images/Visibility.png";
import NotVisibility from "../../source/images/NotVisibility.png";

const Authorization = () => {
  let navigate = useNavigate();

  const [user, setUser] = useState({
    login: "",
    password: ""
  });
  const [errorLogin, setErrorLogin] = useState("");
  const [errorPassword, setErrorPassword] = useState("");
  const [visibilityPassword, setVisibilityPassword] = useState(false);
  
  let flagError = true;

  const visibilityPasswordChange = () => {
    if (visibilityPassword) {
      setVisibilityPassword(false);
    } else {
      setVisibilityPassword(true);
    }
  }

  const toGoReg = () => {
    localStorage.removeItem('authorization');
    navigate("/Registration");
    window.scrollTo(0,0);
  };

  const toGoCheckLoginAndPhoneNamber = () => {
    navigate("/CheckLoginAndPhoneNumberToChangePassword");
    window.scrollTo(0,0);
  };
  
  const authorization = () => {
    setErrorLogin("");
    setErrorPassword("");

    flagError = true;

    if (user.login.length === 0) {
      setErrorLogin("Заполните поле Логин!");
      flagError = false;
    }
    if (user.password.length === 0) {
      setErrorPassword("Заполните поле Пароль!");
      flagError = false;
    }

    if (flagError === true) {
      axios.post('http://localhost:8000/user/authorization', {
        login: user.login,
        password: user.password
      }).then(async res => {
        localStorage.setItem('authorization', JSON.stringify(res.data));

        await setUser({
          login: "",
          password: ""
        });

        navigate("/");
        window.scrollTo(0,0);
      }).catch((err) => {
        if (err.response.status === 421) return setErrorLogin('Такого пользователя не существует!');
        if (err.response.status === 423) return setErrorPassword('Неправильный пароль!');
        if (err.response.status === 422) return setErrorPassword('Неопознанная ошибка!');
      });
    }
  };

  return (
    <div className="container_authorization">
      <p className="heading">Авторизация</p>

      <div className="input_block">
        <div className="input_and_error">
          <input
            className="input"
            maxLength="50"
            type="text"
            onChange={(e) =>
              setUser({
                ...user,
                login: e.target.value,
              })
            }
            value={user.login}
            placeholder="Логин"
          />

          <p className="error_text">
            {errorLogin}
          </p>
        </div>

        <div className="input_and_error">
          <div className="input_password_block">
            <input
              className="input_password"
              maxLength="50"
              type={visibilityPassword ? ("text") :  ("password")}
              onChange={(e) =>
                setUser({
                  ...user,
                  password: e.target.value,
                })
              }
              value={user.password}
              placeholder="Пароль"
            />
            <>
              {!visibilityPassword ? 
                <img src={Visibility} alt="Visibility" onClick={() => visibilityPasswordChange()} />
                :
                <img src={NotVisibility} alt="NotVisibility" onClick={() => visibilityPasswordChange()} />}
            </>
          </div>
          
          <p className="error_text">
            {errorPassword}
          </p>
        </div>
      </div>

      <div className="button_authorization" onClick={() => authorization()}>
        Войти
      </div>

      <a className="forgot_password" onClick={() => toGoCheckLoginAndPhoneNamber()}>
        Забыли пароль?
      </a>

      <a onClick={() => toGoReg()}>
        Регистрация
      </a>
    </div>
  );
};

export default Authorization;