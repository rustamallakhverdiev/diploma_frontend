import React from "react";
import "./Information.scss";
import Logo from "../../source/images/Logo.png";
import LogoBlack from "../../source/images/LogoBlack.png";


const Information = () => {
  const colorTheme = localStorage.getItem('app-theme');

  return (
    <div className="container_information">
      <p className="title">
        О нас
      </p>
      <div className="info_block">
        <img className="logo" src={(colorTheme === 'dark') ? (Logo) : (LogoBlack)} alt="Logo" />
        <div className="text_information">
          Веб-приложение <strong>"Swap"</strong> предназначено для обмена товарами между людьми. <br />
          <strong className="gmail">Swap.help.103@gmail.com</strong> – почтовый индект для обратной связи и вопросов. <br />
          Автор проекта – <strong>Аллахаердиев Рустам</strong> (<a href="https://vk.com/kadimirav">https://vk.com/kadimirav</a>).
        </div>
      </div>
    </div>
  );
};

export default Information;