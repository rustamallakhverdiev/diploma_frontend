import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./AddObject.scss";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";
import AddImg from "../../source/images/AddImg.png";
import DeleteBlue from "../../source/images/DeleteBlue.png";


const AddObject = () => {
  let navigate = useNavigate();

  const categoriesСhoice = [
    {value: '', title: 'Выберите категорию'},
    {value: 'Детские игрушки', title: 'Детские игрушки'},
    {value: 'Настольные игры', title: 'Настольные игры'},
    {value: 'Книги', title: 'Книги'},
    {value: 'Аксессуары', title: 'Аксессуары'},
    {value: 'Одежда', title: 'Одежда'},
    {value: 'Обувь', title: 'Обувь'},
    {value: 'Электроника', title: 'Электроника'},
    {value: 'Инструменты', title: 'Инструменты'},
    {value: 'Мебель', title: 'Мебель'},
    {value: 'Еда', title: 'Еда'},
    {value: 'Недвижимость', title: 'Недвижимость'},
    {value: 'Транспорт', title: 'Транспорт'},
    {value: 'Другое', title: 'Другое'}
  ];

  const categoriesReceiveСhoice = [
    {value: '', title: 'Выберите категорию'},
    {value: 'Детские игрушки', title: 'Детские игрушки'},
    {value: 'Настольные игры', title: 'Настольные игры'},
    {value: 'Книги', title: 'Книги'},
    {value: 'Аксессуары', title: 'Аксессуары'},
    {value: 'Одежда', title: 'Одежда'},
    {value: 'Обувь', title: 'Обувь'},
    {value: 'Электроника', title: 'Электроника'},
    {value: 'Инструменты', title: 'Инструменты'},
    {value: 'Мебель', title: 'Мебель'},
    {value: 'Еда', title: 'Еда'},
    {value: 'Недвижимость', title: 'Недвижимость'},
    {value: 'Транспорт', title: 'Транспорт'},
    {value: 'Другое', title: 'Другое'}
  ];

  const [newObject, setNewObject] = useState({
    title: "",
    categories: "",
    categoriesReceive: "",
    description: ""
  });

  const [errorTitle, setErrorTitle] = useState("");
  const [errorCategories, setErrorCategories] = useState("");
  const [errorCategoriesReceive, setErrorCategoriesReceive] = useState("");
  const [errorDescription, setErrorDescription] = useState("");

  const [loadingImage, setLoadingImage] = useState({
    src: DefaultImgObject,
    file: ''
  });

  let flagError = true;

  const onClickButtonAddObject = () => {
    setErrorTitle("");
    setErrorCategories("");
    setErrorCategoriesReceive("");
    setErrorDescription("");

    flagError = true;

    const fd = new FormData();
    if (loadingImage.file === '') {
      fd.append('imgName', '');
    } else {
      fd.append('imgName', loadingImage.file);
    }

    if (newObject.title.length === 0) {
      setErrorTitle("Заполните поле Название!");
      flagError = false;
    }
    if (newObject.description.length === 0) {
      setErrorDescription("Заполните поле Опиcание!");
      flagError = false;
    }
    if (newObject.categories === "") {
      setErrorCategories("Заполните поле Категория добавляемого товара!");
      flagError = false;
    }
    if (newObject.categoriesReceive === "") {
      setErrorCategoriesReceive("Заполните поле Категория товара на обмен!");
      flagError = false;
    }

    if (flagError === true) {
      const { authorization } = JSON.parse(localStorage.getItem('authorization'));

      axios.post('http://localhost:8000/object/addObject',
      {
        title: newObject.title,
        categories: newObject.categories,
        categoriesReceive: newObject.categoriesReceive,
        description: newObject.description
      },
      {
        headers: { authorization }
      }
      ).then(res => {
        if (loadingImage.file !== '') {
          axios.patch('http://localhost:8000/object/addImgForObject', fd, {
            headers: { id: res.data.data }
          });
        }

        setNewObject({
          title: "",
          categories: "",
          categoriesReceive: "",
          description: ""
        });

        navigate("/MyObjects");
        window.scrollTo(0,0);
      }).catch((err) => {
        if (err.response.status === 421) return setErrorDescription('Вы не авторизированы!');
        if (err.response.status === 422) return setErrorDescription('Неопознанная ошибка!');
      });
    }
  }

  const onClickDeleteImg = async () => {
    await setLoadingImage({
      src: DefaultImgObject,
      file: ''
    });
  }

  const previewImg = async (e) => {
    if (e.target.files[0]) {
      await setLoadingImage({
        src: URL.createObjectURL(e.target.files[0]),
        file: e.target.files[0]
      });
    }
  }

  return (
    <div className="container_add_object">
      <p className="title_page">
        Добавление товара
      </p>

      <div className="main_block">
        <div className="img_block">
          <img
            className="img_object"
            src={loadingImage.src}
            alt="ImgObject" />
          <div className="button_img_block">
            <label>
              <input
                accept="image/*"
                id="contained-button-file"
                name="contained-button-file"
                multiple
                type="file"
                onChange={(e) => previewImg(e)}
                hidden
              />
              <img
                htmlFor="contained-button-file"
                src={AddImg}
                alt="AddImg"
              />
              <p>
                Добавить фото
              </p>
            </label>
            <label>
              <img
                src={DeleteBlue}
                alt="DeleteBlue"
                onClick={() => onClickDeleteImg()}
              />
              <p>
                Удалить все фото
              </p>
            </label>
          </div>
        </div>

        <div className="main_input_block">
          <div className="one_input_block">
            <input
              className="input_title"
              type="text"
              maxLength="50"
              onChange={(e) =>
                setNewObject({
                  ...newObject,
                  title: e.target.value,
                })
              }
              value={newObject.title}
              placeholder="Название"
            />
            <p className="text_error">
              {errorTitle}
            </p>
          </div>

          <div className="one_input_block">
            <select
              className={(newObject.categories === '') ? ("select_categories_first_option") : ("select_categories")}
              onChange={(e) =>
                setNewObject({
                  ...newObject,
                  categories: e.target.value,
                })
              }
              value={newObject.categories}
            >
              {
                categoriesСhoice.map((element, index) => (
                  <option value={element.value} key={`key-${index}`}>{element.title}</option>
                ))
              }
            </select>
            <p className="text_error">
              {errorCategories}
            </p>
          </div>

          <div className="one_input_block">
            <select
              className={(newObject.categoriesReceive === '') ? ("select_categories_first_option") : ("select_categories")}
              onChange={(e) =>
                setNewObject({
                  ...newObject,
                  categoriesReceive: e.target.value,
                })
              }
              value={newObject.categoriesReceive}
            >
              {
                categoriesReceiveСhoice.map((element, index) => (
                  <option value={element.value} key={`key-${index}`}>{element.title}</option>
                ))
              }
            </select>
            <p className="text_error">
              {errorCategoriesReceive}
            </p>
          </div>

          <div className="one_input_block">
            <textarea
              className="input_description"
              type="text"
              onChange={(e) =>
                setNewObject({
                  ...newObject,
                  description: e.target.value,
                })
              }
              value={newObject.description}
              placeholder="Описание"
            />
            <p className="text_error">
              {errorDescription}
            </p>
          </div>
        </div>
      </div>

      <div
        className="button_add_object"
        onClick={() => onClickButtonAddObject()}
      >
        Добавить
      </div>
    </div>
  );
};

export default AddObject;
