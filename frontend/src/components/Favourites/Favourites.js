import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
import "./Favourites.scss";
import DefaultImgObject from "../../source/images/DefaultImgObject.jpg";

const Favourites = () => {
  let navigate = useNavigate();

  const [favouritesObjects, setFavouritesObjects] = useState([]);
  const [useEffectDo, setUseEffectDo] = useState(true);

  useEffect(() => {
    const { authorization } = JSON.parse(localStorage.getItem('authorization'));

    if (useEffectDo) {
      axios.get('http://localhost:8000/favourite/getMyFavourites',
      {
        headers: { authorization }
      }
      ).then(res => {
        const result = res.data.data;
        const newResult = filterMassObject(result);
        setFavouritesObjects(newResult);
        setUseEffectDo(false);
      });
    }
  }, [useEffectDo, setUseEffectDo]);


  const filterMassObject = (mass) => {
    mass = mass.filter((item) => item.object.status);
    return mass;
  };

  const onClickObjectBlock = (id) => {
    navigate(`/ObjectPage/${id}`);
    window.scrollTo(0,0);
  }

  return (
    <div className="container_favourites">
      <p className="title_page">
        Избранное
      </p>
      {
        favouritesObjects.length !== 0 ? (
          <div className="grid_block">
            {
              favouritesObjects.map((element, index) => (
                <div
                  key={`key-${index}`}
                  className={(element.object.status) ? ("object_block") : ("object_block_false")}
                  onClick={() => onClickObjectBlock(element.object.id)}
                >
                  <img
                    src={(element.object.imgName) ? (element.object.imgName) : (DefaultImgObject)}
                    alt="ImgObject"
                    className={(element.object.status) ? ("img_object") : ("img_object_false")}
                  />

                  <div className="div_info">
                    <div>
                      <p className={(element.object.status) ? ("title") : ("title_false")}>
                        {element.object.title}
                      </p>
                      <p className={(element.object.status) ? ("city") : ("city_false")}>
                        {element.object.city}
                      </p>
                    </div>
                  </div>

                  <p className={(element.object.status) ? ("date_add") : ("date_add_false")}>
                    {element.object.date}
                  </p>
                </div>
              ))
            }
          </div>
        )
        :
        (
          <p className="text_error">
            У вас нет избранных товаров!
          </p>
        )
      }
    </div>
  );
};

export default Favourites;