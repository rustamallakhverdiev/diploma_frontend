import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from 'axios';
import "./ChangePasswordByLoginAndPhoneNumber.scss";
import Visibility from "../../source/images/Visibility.png";
import NotVisibility from "../../source/images/NotVisibility.png";

const ChangePasswordByLoginAndPhoneNumber = () => {
  let navigate = useNavigate();
  const { id } = useParams();

  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [visibilityPassword, setVisibilityPassword] = useState(false);

  const visibilityPasswordChange = () => {
    if (visibilityPassword) {
      setVisibilityPassword(false);
    } else {
      setVisibilityPassword(true);
    }
  };

  const onClickSendPassword = () => {
    setPasswordError("");
    let flagError = true;

    if (password.length === 0) {
      setPasswordError("Заполните поле Пароль!");
      flagError = false;
    }

    if (flagError === true) {
      axios.patch('http://localhost:8000/user/changePasswordByLoginAndPhoneNumber',
      {
        id,
        password
      }
      ).then(() => {
        setPasswordError("");
        setPassword("")
        navigate('/Authorization');
        window.scrollTo(0,0);   
      }).catch((err) => {
        if (err.response.status === 423) return setPasswordError('Пароль должен быть длиннее 8 символов!');
        if (err.response.status === 422 || (err.response.status === 400)) return setPasswordError('Неопознанная ошибка!');
      });
    }
  };

  return (
    <section className="container-change-password-by-login-and-phoneNumber">
      <span className="container-change-password-by-login-and-phoneNumber__title">
        Смена пароля
      </span>

      <div className="container-change-password-by-login-and-phoneNumber__input-block input-block">
        <span className="input-block__hint">
          Введите новый пароль:
        </span>
        <div className="input-block__input-password-block input-password-block">
          <input
            className="input-password-block__input-password"
            maxLength="50"
            type={(visibilityPassword) ? ("text") : ("password")}
            onChange={ (e) => setPassword(e.target.value) }
            value={password}
            placeholder="Пароль"
          />
          <>
            {
              (!visibilityPassword) ? (
                <img src={Visibility} alt="Visibility" onClick={ () => visibilityPasswordChange() } />
              ) : (
                <img src={NotVisibility} alt="NotVisibility" onClick={ () => visibilityPasswordChange() } />
              )
            }
          </>
        </div>
        <span className="input-block__error-text">
          {passwordError}
        </span>
      </div>

      <div
        className="container-change-password-by-login-and-phoneNumber__button-send"
        onClick={() => onClickSendPassword()}
      >
        Отправить
      </div>
    </section>
  );
};

export default ChangePasswordByLoginAndPhoneNumber;