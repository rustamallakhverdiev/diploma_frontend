import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Header from "./components/Header/Header.js";
import Registration from "./components/Registration/Registration.js";
import Authorization from "./components/Authorization/Authorization.js";
import Favourites from "./components/Favourites/Favourites.js";
import Information from "./components/Information/Information.js";
import MainPage from "./components/MainPage/MainPage.js";
import MyObjects from "./components/MyObjects/MyObjects.js";
import PersonalAccount from "./components/PersonalAccount/PersonalAccount.js";
import Transactions from "./components/Transactions/Transactions.js";
import Footer from "./components/Footer/Footer.js";
import AddObject from "./components/AddObject/AddObject.js";
import ObjectPage from "./components/ObjectPage/ObjectPage.js";
import EditObject from "./components/EditObject/EditObject.js";
import TransactionPage from "./components/TransactionPage/TransactionPage.js";
import EditUser from "./components/EditUser/EditUser.js";
import ChangePasswordByOldAndNew from "./components/ChangePasswordByOldAndNew/ChangePasswordByOldAndNew.js";
import ChangePasswordByNew from "./components/ChangePasswordByNew/ChangePasswordByNew.js";
import CheckLoginToChangePassword from "./components/CheckLoginToChangePassword/CheckLoginToChangePassword.js";
import CheckLoginAndPhoneNumberToChangePassword from "./components/CheckLoginAndPhoneNumberToChangePassword/CheckLoginAndPhoneNumberToChangePassword.js";
import ChangePasswordByLoginAndPhoneNumber from "./components/ChangePasswordByLoginAndPhoneNumber/ChangePasswordByLoginAndPhoneNumber.js";
import './App.scss';
import { useTheme } from "./hooks/UseTheme.js";

function App() {
  const { theme, setTheme } = useTheme();
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/Registration" element={<Registration />} />
        <Route path="/Authorization" element={<Authorization />} />
        <Route path="/CheckLoginAndPhoneNumberToChangePassword" element={<CheckLoginAndPhoneNumberToChangePassword />} />
        <Route path="/Favourites" element={<Favourites />} />
        <Route path="/Information" element={<Information />} />
        <Route path="/" element={<MainPage />} />
        <Route path="/MyObjects" element={<MyObjects />} />
        <Route path="/PersonalAccount/:id" element={<PersonalAccount />} />
        <Route path="/Transactions" element={<Transactions />} />
        <Route path="/AddObject" element={<AddObject />} />
        <Route path="/ObjectPage/:id" element={<ObjectPage />} />
        <Route path="/EditObject/:id" element={<EditObject />} />
        <Route path="/TransactionPage/:id" element={<TransactionPage />} />
        <Route path="/EditUser" element={<EditUser />} />
        <Route path="/ChangePasswordByOldAndNew" element={<ChangePasswordByOldAndNew />} />
        <Route path="/ChangePasswordByNew" element={<ChangePasswordByNew />} />
        <Route path="/CheckLoginToChangePassword" element={<CheckLoginToChangePassword />} />
        <Route path="/ChangePasswordByLoginAndPhoneNumber/:id" element={<ChangePasswordByLoginAndPhoneNumber />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
